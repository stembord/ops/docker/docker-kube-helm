# stembord/docker-kube-helm:latest

FROM ubuntu:18.04

ARG DOCKER_VERSION=18.06.3
ARG KUBECTL_VERSION=1.15.3-00
ARG HELM_VERSION=2.14.3
ARG TILLER_NAMESPACE=kube-system
ARG HELM_PUSH_URL=https://github.com/chartmuseum/helm-push

ENV HELM_HOST tiller-deploy.${TILLER_NAMESPACE}.svc.cluster.local:44134

RUN apt update
RUN apt install -y \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg-agent \
  software-properties-common

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"
RUN apt install -y apt-transport-https && \
  curl https://packages.cloud.google.com/apt/doc/apt-key.gpg -s | apt-key add - && \
  echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list

RUN apt update

RUN apt install -y \
  docker-ce=$(apt-cache madison docker-ce | grep "${DOCKER_VERSION}" | head -1 | awk '{print $3}') \
  containerd.io

RUN apt-mark hold docker-ce

RUN apt install -y kubelet=${KUBECTL_VERSION} kubeadm=${KUBECTL_VERSION} kubectl=${KUBECTL_VERSION}
RUN apt-mark hold kubelet kubeadm kubectl

RUN apt upgrade -y

RUN curl -s https://storage.googleapis.com/kubernetes-helm/helm-v${HELM_VERSION}-linux-amd64.tar.gz -o helm.tar.gz && \
  tar xf helm.tar.gz && \
  mv linux-amd64/helm /usr/local/bin/ && \
  rm -rf linux-amd64 && \
  rm helm.tar.gz

RUN helm init --client-only
RUN helm plugin install ${HELM_PUSH_URL}

ENTRYPOINT ["/bin/bash"]