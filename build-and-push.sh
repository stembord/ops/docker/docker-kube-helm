#!/bin/bash

repo="stembord/docker-kube-helm"

function build_and_push() {
  local version=$1
  docker build -t ${repo}:${version} .
  docker push ${repo}:${version}
}

build_and_push "latest"